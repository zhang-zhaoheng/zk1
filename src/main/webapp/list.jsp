<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/index_work.css">
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.8.3.js"></script>
    <script type="text/javascript">
        function del(mid) {
            if (mid>0){
                alert("删除成功");
                return "redirect:list";
            }else {
                alert("删除失败");
            }
        }

    </script>
</head>
<body>
<form action="/list" method="post">
    电影名称：<input type="text" name="mname" value="${mname}">
    <input type="submit" value="查询"><br>
    全选：<input type="checkbox" onclick="qx">
    全不选：<input type="checkbox" onclick="bx">
    反选：<input type="checkbox" onclick="fx">
</form>
<table>
    <tr>
        <th><input type="checkbox" onclick="aa"></th>
        <th>编号</th>
        <th>名称</th>
        <th>介绍</th>
        <th>作者</th>
        <th>日期</th>
        <th>类别</th>
        <th>操作</th>
    </tr>
    <c:forEach items="${plist.list}" var="a">
        <tr>
            <td><input type="checkbox" value="${a.mid}" class="c"></td>
            <td>${a.mid}</td>
            <td>${a.mname}</td>
            <td>${a.infos}</td>
            <td>${a.edioer}</td>
            <td>${a.mdate}</td>
            <td>${a.mtype.tname}</td>
            <td><a href="add.jsp">添加</a>
                <a href="update.jsp">修改</a>
            <input type="button" value="删除" onclick="del(mid)">
            </td>
        </tr>
    </c:forEach>
    <tr>
        <td colspan="10">
            当前${plist.pageNum}页 共${plist.pages}页
            <a href="?pageNum=1">首页</a>
            <a href="?pageNum=${plist.prePage}">上一页</a>
            <a href="?pageNum=${plist.nextPage}">下一页</a>
            <a href="?pageNum=${plist.lastPage}">尾页</a>
        </td>
    </tr>
</table>
</body>
</html>
