package com.zhangzhaoheng.service;

import com.zhangzhaoheng.bean.Middle;
import com.zhangzhaoheng.bean.Mov;
import com.zhangzhaoheng.bean.Mtype;
import com.zhangzhaoheng.mapper.MovMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
@Service
public class MovServiceImpl implements  MovService {
    @Resource
    private MovMapper movMapper;
    @Override
    public List<Mov> list(Map map) {
        return movMapper.list(map);
    }

    @Override
    public int add(Mov mov) {
        return movMapper.add(mov);
    }

    @Override
    public int add1(Middle middle) {
        return movMapper.add1(middle);
    }

    @Override
    public List<Mtype> list2() {
        return movMapper.list2();
    }

    @Override
    public int update(Mov mov) {
        return movMapper.update(mov);
    }

    @Override
    public int update2(Middle middle) {
        return movMapper.update2(middle);
    }

    @Override
    public Mov selbyid(Integer mid) {
        return movMapper.selbyid(mid);
    }

    @Override
    public int del(Integer mid) {
        return movMapper.del(mid);
    }

    @Override
    public int del2(Integer id) {
        return movMapper.del2(id);
    }
}
