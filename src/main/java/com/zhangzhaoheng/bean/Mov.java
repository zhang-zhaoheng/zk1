package com.zhangzhaoheng.bean;

public class Mov {
    private  Integer mid;
    private  String mname;
    private  String infos;
    private  String edioer;
    private  String mdate;
    private  Mtype mtype;

    public Mov() {
    }

    public Mov(Integer mid, String mname, String infos, String edioer, String mdate, Mtype mtype) {
        this.mid = mid;
        this.mname = mname;
        this.infos = infos;
        this.edioer = edioer;
        this.mdate = mdate;
        this.mtype = mtype;
    }

    public Integer getMid() {
        return mid;
    }

    public void setMid(Integer mid) {
        this.mid = mid;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getInfos() {
        return infos;
    }

    public void setInfos(String infos) {
        this.infos = infos;
    }

    public String getEdioer() {
        return edioer;
    }

    public void setEdioer(String edioer) {
        this.edioer = edioer;
    }

    public String getMdate() {
        return mdate;
    }

    public void setMdate(String mdate) {
        this.mdate = mdate;
    }

    public Mtype getMtype() {
        return mtype;
    }

    public void setMtype(Mtype mtype) {
        this.mtype = mtype;
    }

    @Override
    public String toString() {
        return "Mov{" +
                "mid=" + mid +
                ", mname='" + mname + '\'' +
                ", infos='" + infos + '\'' +
                ", edioer='" + edioer + '\'' +
                ", mdate='" + mdate + '\'' +
                ", mtype=" + mtype +
                '}';
    }
}
