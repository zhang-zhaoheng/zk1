package com.zhangzhaoheng.bean;

import java.util.List;

public class Mtype {
    private  Integer tid;
    private  String tname;
    private List<Mov> mlist;

    public Mtype() {
    }

    public Mtype(Integer tid, String tname, List<Mov> mlist) {
        this.tid = tid;
        this.tname = tname;
        this.mlist = mlist;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public List<Mov> getMlist() {
        return mlist;
    }

    public void setMlist(List<Mov> mlist) {
        this.mlist = mlist;
    }

    @Override
    public String toString() {
        return "Mtype{" +
                "tid=" + tid +
                ", tname='" + tname + '\'' +
                ", mlist=" + mlist +
                '}';
    }
}
