package com.zhangzhaoheng.bean;

public class Middle {
    private  Integer id;
    private  Integer mid;
    private  Integer tid;

    public Middle() {
    }

    public Middle(Integer id, Integer mid, Integer tid) {
        this.id = id;
        this.mid = mid;
        this.tid = tid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMid() {
        return mid;
    }

    public void setMid(Integer mid) {
        this.mid = mid;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    @Override
    public String toString() {
        return "Middle{" +
                "id=" + id +
                ", mid=" + mid +
                ", tid=" + tid +
                '}';
    }
}
