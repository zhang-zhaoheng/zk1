package com.zhangzhaoheng.mapper;

import com.zhangzhaoheng.bean.Middle;
import com.zhangzhaoheng.bean.Mov;
import com.zhangzhaoheng.bean.Mtype;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface MovMapper {
    List<Mov> list(Map map);
    int add(Mov mov);
    int add1(Middle middle);
    List<Mtype> list2();
    int update(Mov mov);
    int update2(Middle middle);
    Mov selbyid(@Param("mid")Integer mid);
    int del(@Param("mid") Integer mid);
    int del2(@Param("id") Integer id);
}
