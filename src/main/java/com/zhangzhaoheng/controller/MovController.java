package com.zhangzhaoheng.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhangzhaoheng.bean.Middle;
import com.zhangzhaoheng.bean.Mov;
import com.zhangzhaoheng.bean.Mtype;
import com.zhangzhaoheng.service.MovService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class MovController {
    @Resource
    private MovService movService;
    //列表
    @RequestMapping("list")
    public  String list(Model model, @RequestParam(defaultValue = "1")Integer pageNum, String mname){
        PageHelper.startPage(pageNum,2);
        Map map=new HashMap();
        map.put("mname",mname);
        List<Mov> mlist = movService.list(map);
        PageInfo<Mov> plist = new PageInfo<>(mlist);
        model.addAttribute("plist",plist);
        model.addAttribute("mname",mname);
        return  "list";
    }
    //主表添加
    @RequestMapping("add")
    public  String add(Mov mov){
        try {
            movService.add(mov);
            return  "redirect:list";
        }catch (Exception e){
            e.printStackTrace();
            return  "add";
        }
    }
    //添加中间表
    @RequestMapping("add1")
    @ResponseBody
    public  String add1(Middle middle){
        movService.add1(middle);
        return  "redirect:list";

    }
    //类别表的查询
    @RequestMapping("sel")
    @ResponseBody
    public  List<Mtype> list2(){
        List<Mtype> mtypes = movService.list2();
        return  mtypes;
    }
    //主表的修改
    @RequestMapping("update")
    public  String update(Mov mov){
        try {
            movService.update(mov);
            return  "redirect:list";
        }catch (Exception e){
            e.printStackTrace();
            return  "update";
        }
    }
    //中间表的修改
    @RequestMapping("update2")
    @ResponseBody
    public  String update2(Middle middle){
        movService.update2(middle);
        return  "redirect:list";

    }
    //回显
    @RequestMapping("huix")
    @ResponseBody
    public  Mov huix(@RequestParam("mid")Integer mid){
        Mov mov = movService.selbyid(mid);
        return  mov;
    }
    //删除
    @RequestMapping("del")
    @ResponseBody
    public  boolean del(Integer mid){
        int del = movService.del(mid);
        if(del>0){
            return  true;
        }else {
            return  false;
        }
    }
}
